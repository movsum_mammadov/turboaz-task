package az.ingress.turboaz.dto.response;

import az.ingress.turboaz.domain.BrandEntity;
import az.ingress.turboaz.domain.ModelEntity;
import az.ingress.turboaz.domain.SellerEntity;
import az.ingress.turboaz.enums.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class VehicleResponseDto {

    Long id;

    BrandEntity brand;

    ModelEntity model;

    Double price;

    CurrencyEnum currency;

    Boolean isCredit;

    Boolean isBarter;

    BanTypeEnum banType;

    Integer year;

    ColorEnum color;

    FuelTypeEnum fuelType;

    TransmissionEnum transmission;

    GearEnum gear;

    EngineVolumeEnum engineVolume;
    Double engine;


    Double horsePower;

    Long mileage;

    SellerEntity seller;

    OwnerCountEnum ownerCount;

    NumOfSeatsEnum numOfSeats;

    MarketPlaceEnum marketPlace;

    Boolean accident;
    Boolean isPainted;
    Boolean wrecked;

    StatusEnum status;

    String vinCode;

    Boolean alloyWheels;
    Boolean ABS;
    Boolean rainSensor;
    Boolean centralLocking;
    Boolean parkingRadar;
    Boolean airConditioner;
    Boolean heatedSeats;
    Boolean leatherInterior;
    Boolean ventilatedSeats;

    LocalDate createdDate;

    Long viewCount;

}
