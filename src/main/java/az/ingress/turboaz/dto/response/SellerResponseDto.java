package az.ingress.turboaz.dto.response;

import az.ingress.turboaz.enums.CityEnum;
import az.ingress.turboaz.enums.SellerTypeEnum;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SellerResponseDto {

    Long id;

    String name;

    String phone;

    SellerTypeEnum sellerType;

    CityEnum city;
}
