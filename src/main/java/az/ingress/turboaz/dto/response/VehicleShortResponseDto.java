package az.ingress.turboaz.dto.response;

import az.ingress.turboaz.enums.CityEnum;
import az.ingress.turboaz.enums.CurrencyEnum;
import az.ingress.turboaz.enums.EngineVolumeEnum;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class VehicleShortResponseDto {

    Long id;

    Long brandId;

    Long modelId;

//    BrandResponseDto brandResponseDto;
//
//    ModelResponseDto modelResponseDto;

    Integer year;

    CityEnum city;

    Double price;

    CurrencyEnum currency;

    Double engine;

    Long mileage;

    LocalDate createdDate;
}
