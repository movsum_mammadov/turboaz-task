package az.ingress.turboaz.dto.request;


import az.ingress.turboaz.enums.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class VehicleRequestDto {

    Long brand_id;

    Long model_id;

    Double price;

    CurrencyEnum currency;

    Boolean isCredit;

    Boolean isBarter;

    BanTypeEnum banType;

    Integer year;

    ColorEnum color;

    FuelTypeEnum fuelType;

    TransmissionEnum transmission;

    GearEnum gear;

    EngineVolumeEnum engineVolume;

    Double engine;

    Double horsePower;

    Long mileage;

    Long seller_id;

    OwnerCountEnum ownerCount;

    NumOfSeatsEnum numOfSeats;

    MarketPlaceEnum marketPlace;

    Boolean accident;
    Boolean isPainted;
    Boolean wrecked;

    StatusEnum status;

    String vinCode;

    Boolean alloyWheels;
    Boolean ABS;
    Boolean rainSensor;
    Boolean centralLocking;
    Boolean parkingRadar;
    Boolean airConditioner;
    Boolean heatedSeats;
    Boolean leatherInterior;
    Boolean ventilatedSeats;


}
