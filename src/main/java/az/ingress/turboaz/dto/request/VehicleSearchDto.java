package az.ingress.turboaz.dto.request;

import az.ingress.turboaz.enums.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class VehicleSearchDto {

    Long brandId;

    Long modelId;

    Double minPrice;
    Double maxPrice;

    CurrencyEnum currency;

    Boolean isCredit;

    Boolean isBarter;

    BanTypeEnum banType;

    Integer minYear;
    Integer maxYear;


    ColorEnum color;

    FuelTypeEnum fuelType;

    TransmissionEnum transmission;

//    GearEnum gear;
//
//    EngineVolumeEnum engineVolume;
//    Double engine;
//
//    Double minHorsePower;
//    Double mHorsePower;
//
//    Long mileage;
//
//    Long sellerId;
//
//    OwnerCountEnum ownerCount;
//
//    NumOfSeatsEnum numOfSeats;
//
//    MarketPlaceEnum marketPlace;
//
//    Boolean accident;
//    Boolean isPainted;
//    Boolean wrecked;
//
//    StatusEnum status;
//
//    String vinCode;
//
//    Boolean alloyWheels;
//    Boolean ABS;
//    Boolean rainSensor;
//    Boolean centralLocking;
//    Boolean parkingRadar;
//    Boolean airConditioner;
//    Boolean heatedSeats;
//    Boolean leatherInterior;
//    Boolean ventilatedSeats;
}
