package az.ingress.turboaz.dto.request;

import az.ingress.turboaz.enums.CityEnum;
import az.ingress.turboaz.enums.SellerTypeEnum;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SellerRequestDto {

    String name;

    String phone;

    SellerTypeEnum sellerType;

    CityEnum city;

}
