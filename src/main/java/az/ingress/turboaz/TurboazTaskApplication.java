package az.ingress.turboaz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TurboazTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(TurboazTaskApplication.class, args);
    }

}
