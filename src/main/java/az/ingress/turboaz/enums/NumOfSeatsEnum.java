package az.ingress.turboaz.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public enum NumOfSeatsEnum {

    ONE("1"), TWO("2"), THREE("3"),
    FOUR("4"), FIVE("5"), SIX("6"),
    SEVEN("7"), EIGHT_AND_MORE("8+");

    String value;

    NumOfSeatsEnum(String value) {
        this.value = value;
    }

}
