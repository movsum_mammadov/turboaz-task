package az.ingress.turboaz.enums;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public enum CurrencyEnum {

    AZN("AZN"), EUR("EUR"), USD("USD");

    String value;

    CurrencyEnum(String value) {
        this.value = value;
    }
}