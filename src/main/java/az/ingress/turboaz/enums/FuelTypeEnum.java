package az.ingress.turboaz.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public enum FuelTypeEnum {
    PETROL("Petrol"),
    DIESEL("Diesel"),
    NATURAL_GAS("Natural Gas"),
    ELECTRIC("Electric"),
    HYBRID("Hybrid"),
    PLUG_IN_HYBRID("Plug in Hybrid");

    String value;

    FuelTypeEnum(String value) {
        this.value = value;
    }
}
