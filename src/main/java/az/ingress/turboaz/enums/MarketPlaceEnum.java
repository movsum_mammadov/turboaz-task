package az.ingress.turboaz.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public enum MarketPlaceEnum {

    AMERICA("America"), EUROPE("Europe"), CHINA("China"),
    OTHER("Other"), DUBAI("Dubai"), KOREA("Korea"),
    OFFICIAL_DEALER("Official Dealer"), RUSSIA("Russia"), JAPAN("Japan");

    String value;

    MarketPlaceEnum(String value) {
        this.value = value;
    }
}
