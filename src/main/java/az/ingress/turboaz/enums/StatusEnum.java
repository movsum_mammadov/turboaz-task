package az.ingress.turboaz.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public enum StatusEnum {

    IN_STOCK("In Stock"), BY_ORDER("By Order");

    String value;

    StatusEnum(String value) {
        this.value = value;
    }
}
