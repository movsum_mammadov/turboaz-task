package az.ingress.turboaz.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public enum GearEnum {

    MANUAL("Manual"), AUTOMATIC("Automatic"),
    ROBOTISED("Robotised"), VARIATOR("Variator");

    String value;

    GearEnum(String value) {
        this.value = value;
    }
}

