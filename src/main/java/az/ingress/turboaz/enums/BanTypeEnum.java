package az.ingress.turboaz.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public enum BanTypeEnum {
    BUS("Bus"), HATCHBACK("Hatchback"), CONVERTIBLE("Convertible"),
    CARAVAN("Caravan"), COUPE("Coupe"), MINIVAN("Minivan"),
    MOPED("Moped"), MOTORCYCLE("Motorcycle"), SUV("SUV"),
    PICKUP("Pickup"), GOLF_CART("Golf cart"), ROADSTER("Roadster"),
    SEDAN("Sedan"), VAN("Van"), TRUCK("Truck");

    String value;

    BanTypeEnum(String value) {
        this.value = value;
    }
}
