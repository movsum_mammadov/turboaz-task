package az.ingress.turboaz.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public enum SellerTypeEnum {

    SALON("Salon"), PERSONAL("Personal");

    String value;

    SellerTypeEnum(String value) {
        this.value = value;
    }
}
