package az.ingress.turboaz.enums;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public enum ColorEnum {

    BLACK("Black"), WET_ASPHALT("Wet asphalt"), GRAY("Gray"),
    SILVER("Silver"), WHITE("White"), BEIGE("Beige"),
    RED("Red"), PINK("Pink"), ORANGE("Orange"),
    GOLD("Gold"), YELLOW("Yellow"), GREEN("Green"),
    LIGHT_GREEN("Light green"), BLUE("Blue"),
    PURPLE("Purple"), BROWN("Brown");

    String value;

    ColorEnum(String value) {
        this.value = value;
    }
}
