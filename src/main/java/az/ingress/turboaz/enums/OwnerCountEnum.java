package az.ingress.turboaz.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public enum OwnerCountEnum {

    ONE("1"), TWO("2"),
    THREE("3"), FOUR_AND_MORE("4+");

    String value;

    OwnerCountEnum(String value) {
        this.value = value;
    }

}
