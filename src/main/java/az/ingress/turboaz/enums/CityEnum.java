package az.ingress.turboaz.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public enum CityEnum {
    BARDA("Barda"), ABSHERON("Absheron"), AGHJABADI("Aghjabadi"),
    AGHDAM("Aghdam"), AGHDASH("Agdash"), AGHSTAFA("Aghstafa"),
    AGSU("Agsu"), ASTARA("Astara"), BABEK("Babek"),
    BAKU("Baku"), BALAKAN("Balakan"), BEYLAGAN("Beylagan"),
    BILASUVAR("Bilasuvar"), JABRAYIL("Jabrayil"), JALILABAD("Jalilabad");

    String value;

    CityEnum(String value) {
        this.value = value;
    }
}
