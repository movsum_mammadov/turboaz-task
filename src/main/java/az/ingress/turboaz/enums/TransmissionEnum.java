package az.ingress.turboaz.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public enum TransmissionEnum {
    FRONT("Front"), BACK("Back"), FULL("Full");

    String value;

    TransmissionEnum(String value) {
        this.value = value;
    }
}
