package az.ingress.turboaz.repository;

import az.ingress.turboaz.domain.SellerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SellerRepository extends JpaRepository<SellerEntity,Long> {
}
