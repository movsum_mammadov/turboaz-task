package az.ingress.turboaz.repository;

import az.ingress.turboaz.domain.ModelEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ModelRepository extends JpaRepository<ModelEntity, Long> {
}
