package az.ingress.turboaz.repository;

import az.ingress.turboaz.domain.VehicleEntity;
import az.ingress.turboaz.dto.response.VehicleShortResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface VehicleRepository extends JpaRepository<VehicleEntity, Long> ,
        JpaSpecificationExecutor<VehicleEntity> {

    Page<VehicleEntity> findAllByOrderByCreatedDateDesc(Pageable pageable);

    Page<VehicleEntity> findAllByOrderByPriceDesc(Pageable pageable);

    Page<VehicleEntity> findAllByOrderByPriceAsc(Pageable pageable);

    Page<VehicleEntity> findAllByOrderByMileageAsc(Pageable pageable);

    Page<VehicleEntity> findAllByOrderByYearDesc(Pageable pageable);

}
