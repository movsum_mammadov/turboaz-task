package az.ingress.turboaz.service.impl;

import az.ingress.turboaz.domain.SellerEntity;
import az.ingress.turboaz.dto.request.SellerRequestDto;
import az.ingress.turboaz.dto.response.SellerResponseDto;
import az.ingress.turboaz.repository.SellerRepository;
import az.ingress.turboaz.service.SellerService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class SellerServiceImpl implements SellerService {

    SellerRepository sellerRepository;

    ModelMapper mapper;

    @Override
    public List<SellerResponseDto> getAllSellers() {
        return sellerRepository.findAll()
                .stream()
                .map(sellerEntity -> mapper.map(sellerEntity,SellerResponseDto.class))
                .toList();
    }

    @Override
    public SellerResponseDto getSellerById(Long id) {
        SellerEntity seller=sellerRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Seller with id %d not found".formatted(id)));
        return mapper.map(seller,SellerResponseDto.class);
    }

    @Override
    public SellerResponseDto createSeller(SellerRequestDto sellerRequestDto) {
        SellerEntity seller=mapper.map(sellerRequestDto,SellerEntity.class);
        return mapper.map(sellerRepository.save(seller), SellerResponseDto.class);
    }

    @Override
    public SellerResponseDto updateSeller(Long id, SellerRequestDto sellerRequestDto) {
        sellerRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Seller with id %d not found".formatted(id)));
        SellerEntity seller = mapper.map(sellerRequestDto,SellerEntity.class);
        seller.setId(id);
        return mapper.map(sellerRepository.save(seller),SellerResponseDto.class);
    }

    @Override
    public void deleteSellerById(Long id) {
        sellerRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Seller with id %d not found".formatted(id)));
        sellerRepository.deleteById(id);
    }
}
