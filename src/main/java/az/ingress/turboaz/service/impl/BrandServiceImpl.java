package az.ingress.turboaz.service.impl;

import az.ingress.turboaz.domain.BrandEntity;
import az.ingress.turboaz.domain.SellerEntity;
import az.ingress.turboaz.dto.request.BrandRequestDto;
import az.ingress.turboaz.dto.response.BrandResponseDto;
import az.ingress.turboaz.dto.response.SellerResponseDto;
import az.ingress.turboaz.repository.BrandRepository;
import az.ingress.turboaz.service.BrandService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class BrandServiceImpl implements BrandService {

    BrandRepository brandRepository;

    ModelMapper mapper;

    @Override
    public List<BrandResponseDto> getAllBrands() {
        return brandRepository.findAll()
                .stream()
                .map(brandEntity -> mapper.map(brandEntity,BrandResponseDto.class))
                .toList();
    }

    @Override
    public BrandResponseDto getBrandById(Long id) {
        BrandEntity brand = brandRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Brand with id %d not found".formatted(id)));
        return mapper.map(brand,BrandResponseDto.class);
    }

    @Override
    public BrandResponseDto createBrand(BrandRequestDto brandRequestDto) {
        BrandEntity brand = mapper.map(brandRequestDto, BrandEntity.class);
        return mapper.map(brandRepository.save(brand),BrandResponseDto.class);
    }

    @Override
    public BrandResponseDto updateBrand(Long id, BrandRequestDto brandRequestDto) {
        brandRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Brand with id %d not found".formatted(id)));
        BrandEntity brand = mapper.map(brandRequestDto,BrandEntity.class);
        brand.setId(id);
        return mapper.map(brandRepository.save(brand), BrandResponseDto.class);
    }


    @Override
    public void deleteBrandById(Long id) {
        brandRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Brand with id %d not found".formatted(id)));
        brandRepository.deleteById(id);
    }
}
