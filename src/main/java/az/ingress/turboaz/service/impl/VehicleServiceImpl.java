package az.ingress.turboaz.service.impl;

import az.ingress.turboaz.domain.BrandEntity;
import az.ingress.turboaz.domain.ModelEntity;
import az.ingress.turboaz.domain.SellerEntity;
import az.ingress.turboaz.domain.VehicleEntity;
import az.ingress.turboaz.dto.request.VehicleRequestDto;
import az.ingress.turboaz.dto.request.VehicleSearchDto;
import az.ingress.turboaz.dto.response.VehicleResponseDto;
import az.ingress.turboaz.dto.response.VehicleShortResponseDto;
import az.ingress.turboaz.repository.BrandRepository;
import az.ingress.turboaz.repository.ModelRepository;
import az.ingress.turboaz.repository.SellerRepository;
import az.ingress.turboaz.repository.VehicleRepository;
import az.ingress.turboaz.service.VehicleService;
import jakarta.persistence.criteria.Predicate;
import jakarta.transaction.Transactional;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class VehicleServiceImpl implements VehicleService {

    VehicleRepository vehicleRepository;

    BrandRepository brandRepository;

    SellerRepository sellerRepository;

    ModelRepository modelRepository;

    ModelMapper mapper;

    public Page<VehicleResponseDto> getAllVehicles(Pageable pageable) {
        return vehicleRepository.findAll(pageable)
                .map(vehicleEntity -> mapper.map(vehicleEntity, VehicleResponseDto.class));
    }

    @Override
    public VehicleResponseDto getVehicleById(Long id) {
        VehicleEntity vehicle = vehicleRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Vehicle with id %d not found".formatted(id)));
        vehicle.setViewCount(vehicle.getViewCount() + 1);
        vehicleRepository.save(vehicle);
        return mapper.map(vehicle, VehicleResponseDto.class);
    }

    @Override
    public VehicleResponseDto createVehicle(VehicleRequestDto vehicleRequestDto) {
        VehicleEntity vehicle = mapper.map(vehicleRequestDto, VehicleEntity.class);
//        System.out.println(vehicleRequestDto.getBrand_id());
        BrandEntity brand = brandRepository.findById(vehicleRequestDto.getBrand_id())
                .orElseThrow(() -> new RuntimeException("Brand with id %d not found"
                        .formatted(vehicleRequestDto.getBrand_id())));
        vehicle.setBrand(brand);
        SellerEntity seller = sellerRepository.findById(vehicleRequestDto.getSeller_id())
                .orElseThrow(() -> new RuntimeException("Seller with id %d not found"
                        .formatted(vehicleRequestDto.getSeller_id())));
        vehicle.setSeller(seller);
        ModelEntity model = modelRepository.findById(vehicleRequestDto.getModel_id())
                .orElseThrow(() -> new RuntimeException("Model with id %d not found"
                        .formatted(vehicleRequestDto.getModel_id())));
        vehicle.setModel(model);
        vehicle.setViewCount(0L);
        vehicle.setCreatedDate(LocalDate.now());
        return mapper.map(vehicleRepository.save(vehicle), VehicleResponseDto.class);
    }

    @Override
    public VehicleResponseDto updateVehicle(Long id, VehicleRequestDto vehicleRequestDto) {
        vehicleRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Vehicle with id %d not found".formatted(id)));
        VehicleEntity vehicle = mapper.map(vehicleRequestDto, VehicleEntity.class);
        vehicle.setId(id);
        return mapper.map(vehicle, VehicleResponseDto.class);
    }

    @Override
    public void deleteVehicleById(Long id) {
        vehicleRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Vehicle with id %d not found".formatted(id)));
        vehicleRepository.deleteById(id);
    }

    @Override
    public Page<VehicleShortResponseDto> search(Pageable pageable, VehicleSearchDto vehicleSearchDto) {
        Specification<VehicleEntity> specification = (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (vehicleSearchDto.getBrandId() != null) {
                predicates.add(cb.equal(root.get(VehicleEntity.Fields.brand).get("id"), vehicleSearchDto.getBrandId()));
            }
            if (vehicleSearchDto.getModelId() != null) {
                predicates.add(cb.equal(root.get(VehicleEntity.Fields.model).get("id"), vehicleSearchDto.getModelId()));
            }
            if (vehicleSearchDto.getMinPrice() != null) {
                predicates.add(cb.equal(root.get(VehicleEntity.Fields.price), vehicleSearchDto.getMinPrice()));
            }
            if (vehicleSearchDto.getMaxPrice() != null) {
                predicates.add(cb.equal(root.get(VehicleEntity.Fields.price), vehicleSearchDto.getMaxPrice()));
            }
            if (vehicleSearchDto.getCurrency() != null) {
                predicates.add(cb.equal(root.get(VehicleEntity.Fields.currency), vehicleSearchDto.getCurrency()));
            }
            if (vehicleSearchDto.getIsCredit() != null) {
                predicates.add(cb.equal(root.get(VehicleEntity.Fields.isCredit), vehicleSearchDto.getIsCredit()));
            }
            if (vehicleSearchDto.getIsBarter() != null) {
                predicates.add(cb.equal(root.get(VehicleEntity.Fields.isBarter), vehicleSearchDto.getIsBarter()));
            }
            if (vehicleSearchDto.getBanType() != null) {
                predicates.add(cb.equal(root.get(VehicleEntity.Fields.banType), vehicleSearchDto.getBanType()));
            }
            if (vehicleSearchDto.getMinYear() != null) {
                predicates.add(cb.equal(root.get(VehicleEntity.Fields.year), vehicleSearchDto.getMinYear()));
            }
            if (vehicleSearchDto.getMaxPrice() != null) {
                predicates.add(cb.equal(root.get(VehicleEntity.Fields.year), vehicleSearchDto.getMaxYear()));
            }
            if (vehicleSearchDto.getColor() != null) {
                predicates.add(cb.equal(root.get(VehicleEntity.Fields.color), vehicleSearchDto.getColor()));
            }
            if (vehicleSearchDto.getFuelType() != null) {
                predicates.add(cb.equal(root.get(VehicleEntity.Fields.fuelType), vehicleSearchDto.getFuelType()));
            }
            if (vehicleSearchDto.getTransmission() != null) {
                predicates.add(cb.equal(root.get(VehicleEntity.Fields.transmission), vehicleSearchDto.getTransmission()));
            }
            return cb.and(predicates.toArray(new Predicate[0]));
        };
        return vehicleRepository.findAll(specification, pageable)
                .map(vehicleEntity -> mapper.map(vehicleEntity, VehicleShortResponseDto.class));
    }

    @Override
    public Page<VehicleShortResponseDto> getVehicleSortedByCreatedDateDesc(Pageable pageable) {
        return vehicleRepository.findAllByOrderByCreatedDateDesc(pageable)
                .map(vehicleEntity -> mapper.map(vehicleEntity,VehicleShortResponseDto.class));
    }

    @Override
    public Page<VehicleShortResponseDto> getVehicleSortedByPriceDesc(Pageable pageable) {
        return vehicleRepository.findAllByOrderByPriceDesc(pageable)
                .map(vehicleEntity -> mapper.map(vehicleEntity,VehicleShortResponseDto.class));
    }

    @Override
    public Page<VehicleShortResponseDto> getVehicleSortedByPriceAsc(Pageable pageable) {
        return vehicleRepository.findAllByOrderByPriceAsc(pageable)
                .map(vehicleEntity -> mapper.map(vehicleEntity, VehicleShortResponseDto.class));
    }

    @Override
    public Page<VehicleShortResponseDto> getVehicleSortedByMileageAsc(Pageable pageable) {
        return vehicleRepository.findAllByOrderByMileageAsc(pageable)
                .map(vehicleEntity -> mapper.map(vehicleEntity,VehicleShortResponseDto.class));
    }

    @Override
    public Page<VehicleShortResponseDto> getVehicleSortedByYearDesc(Pageable pageable) {
        return vehicleRepository.findAllByOrderByYearDesc(pageable)
                .map(vehicleEntity -> mapper.map(vehicleEntity, VehicleShortResponseDto.class));
    }


}
