package az.ingress.turboaz.service.impl;

import az.ingress.turboaz.domain.ModelEntity;
import az.ingress.turboaz.dto.request.ModelRequestDto;
import az.ingress.turboaz.dto.response.ModelResponseDto;
import az.ingress.turboaz.repository.ModelRepository;
import az.ingress.turboaz.service.ModelService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ModelServiceImpl implements ModelService {

    ModelRepository modelRepository;

    ModelMapper mapper;

    @Override
    public List<ModelResponseDto> getAllModels() {
        return modelRepository.findAll()
                .stream()
                .map(modelEntity -> mapper.map(modelEntity, ModelResponseDto.class))
                .toList();
    }

    @Override
    public ModelResponseDto getModelById(Long id) {
        ModelEntity model = modelRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Model with id %d not found".formatted(id)));
        return mapper.map(model, ModelResponseDto.class);
    }

    @Override
    public ModelResponseDto createModel(ModelRequestDto modelRequestDto) {
        ModelEntity model = mapper.map(modelRequestDto, ModelEntity.class);
        return mapper.map(modelRepository.save(model), ModelResponseDto.class);
    }

    @Override
    public ModelResponseDto updateModel(Long id, ModelRequestDto modelRequestDto) {
        modelRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Model with id %d not found".formatted(id)));
        ModelEntity model = mapper.map(modelRequestDto, ModelEntity.class);
        model.setId(id);
        return mapper.map(modelRepository.save(model), ModelResponseDto.class);
    }

    @Override
    public void deleteModelById(Long id) {
        modelRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Model with id %d not found".formatted(id)));
        modelRepository.deleteById(id);
    }
}
