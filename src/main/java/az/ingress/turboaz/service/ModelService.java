package az.ingress.turboaz.service;

import az.ingress.turboaz.dto.request.BrandRequestDto;
import az.ingress.turboaz.dto.request.ModelRequestDto;
import az.ingress.turboaz.dto.request.SellerRequestDto;
import az.ingress.turboaz.dto.response.BrandResponseDto;
import az.ingress.turboaz.dto.response.ModelResponseDto;
import az.ingress.turboaz.dto.response.SellerResponseDto;

import java.util.List;

public interface ModelService {

    List<ModelResponseDto> getAllModels();

    ModelResponseDto getModelById(Long id);

    ModelResponseDto createModel(ModelRequestDto modelRequestDto);

    ModelResponseDto updateModel(Long id, ModelRequestDto modelRequestDto);

    void deleteModelById(Long id);
}
