package az.ingress.turboaz.service;

import az.ingress.turboaz.dto.request.SellerRequestDto;
import az.ingress.turboaz.dto.response.ModelResponseDto;
import az.ingress.turboaz.dto.response.SellerResponseDto;

import java.util.List;

public interface SellerService {

    List<SellerResponseDto> getAllSellers();

    SellerResponseDto getSellerById(Long id);

    SellerResponseDto createSeller(SellerRequestDto sellerRequestDto);

    SellerResponseDto updateSeller(Long id, SellerRequestDto sellerRequestDto);

    void deleteSellerById(Long id);
}
