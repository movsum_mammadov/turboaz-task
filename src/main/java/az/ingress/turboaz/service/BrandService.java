package az.ingress.turboaz.service;

import az.ingress.turboaz.dto.request.BrandRequestDto;
import az.ingress.turboaz.dto.request.SellerRequestDto;
import az.ingress.turboaz.dto.response.BrandResponseDto;
import az.ingress.turboaz.dto.response.SellerResponseDto;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface BrandService {

    List<BrandResponseDto> getAllBrands();

    BrandResponseDto getBrandById(Long id);

    BrandResponseDto createBrand(BrandRequestDto brandRequestDto);

    BrandResponseDto updateBrand(Long id, BrandRequestDto brandRequestDto);

    void deleteBrandById(Long id);

}
