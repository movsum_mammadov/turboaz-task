package az.ingress.turboaz.service;

import az.ingress.turboaz.dto.request.VehicleRequestDto;
import az.ingress.turboaz.dto.request.VehicleSearchDto;
import az.ingress.turboaz.dto.response.VehicleResponseDto;
import az.ingress.turboaz.dto.response.VehicleShortResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface VehicleService {

    Page<VehicleResponseDto> getAllVehicles(Pageable pageable);

    VehicleResponseDto getVehicleById(Long id);

    VehicleResponseDto createVehicle(VehicleRequestDto vehicleRequestDto);

    VehicleResponseDto updateVehicle(Long id, VehicleRequestDto vehicleRequestDto);

    void deleteVehicleById(Long id);

    Page<VehicleShortResponseDto> search(Pageable pageable, VehicleSearchDto vehicleSearchDto);

    Page<VehicleShortResponseDto> getVehicleSortedByCreatedDateDesc(Pageable pageable);

    Page<VehicleShortResponseDto> getVehicleSortedByPriceDesc(Pageable pageable);

    Page<VehicleShortResponseDto> getVehicleSortedByPriceAsc(Pageable pageable);

    Page<VehicleShortResponseDto> getVehicleSortedByMileageAsc(Pageable pageable);

    Page<VehicleShortResponseDto> getVehicleSortedByYearDesc(Pageable pageable);
}
