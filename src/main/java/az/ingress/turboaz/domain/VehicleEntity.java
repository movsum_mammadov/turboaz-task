package az.ingress.turboaz.domain;


import az.ingress.turboaz.enums.*;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import lombok.experimental.FieldNameConstants;

import java.time.LocalDate;

@Data
@Entity
@Builder
@Table(name = "vehicles")
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
@FieldDefaults(level = AccessLevel.PRIVATE)
public class VehicleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne
    @JoinColumn(name = "brand_id")
    BrandEntity brand;

    @ManyToOne
    @JoinColumn(name = "model_id")
    ModelEntity model;

    Double price;

    @Enumerated(EnumType.STRING)
    CurrencyEnum currency;

    Boolean isCredit;

    Boolean isBarter;

    @Enumerated(EnumType.STRING)
    BanTypeEnum banType;

    Integer year;

    @Enumerated(EnumType.STRING)
    ColorEnum color;

    @Enumerated(EnumType.STRING)
    FuelTypeEnum fuelType;

    @Enumerated(EnumType.STRING)
    TransmissionEnum transmission;

    @Enumerated(EnumType.STRING)
    GearEnum gear;

    @Enumerated(EnumType.STRING)
    EngineVolumeEnum engineVolume;

    Double engine;

    Double horsePower;

    Long mileage;

    @ManyToOne
    @JoinColumn(name = "seller_id")
    SellerEntity seller;

    @Enumerated(EnumType.STRING)
    OwnerCountEnum ownerCount;

    @Enumerated(EnumType.STRING)
    NumOfSeatsEnum numOfSeats;

    @Enumerated(EnumType.STRING)
    MarketPlaceEnum marketPlace;

    Boolean accident;
    Boolean isPainted;
    Boolean wrecked;

    @Enumerated(EnumType.STRING)
    StatusEnum status;

    String vinCode;

    Boolean alloyWheels;
    Boolean ABS;
    Boolean rainSensor;
    Boolean centralLocking;
    Boolean parkingRadar;
    Boolean airConditioner;
    Boolean heatedSeats;
    Boolean leatherInterior;
    Boolean ventilatedSeats;

    LocalDate createdDate;

    Long viewCount;


}
