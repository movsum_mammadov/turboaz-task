package az.ingress.turboaz.domain;


import az.ingress.turboaz.enums.CityEnum;
import az.ingress.turboaz.enums.SellerTypeEnum;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@Entity
@Builder
@Table(name="sellers")
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SellerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;

    String phone;

    @Enumerated(EnumType.STRING)
    SellerTypeEnum sellerType;

    @JsonBackReference
    @OneToMany(mappedBy = "seller",cascade = CascadeType.ALL)
    List<VehicleEntity> vehicles;

    @Enumerated(EnumType.STRING)
    CityEnum city;
}
