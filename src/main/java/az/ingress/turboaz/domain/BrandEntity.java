package az.ingress.turboaz.domain;


import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@Entity
@Builder
@Table(name="brands")
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BrandEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;

    @JsonBackReference
    @OneToMany(mappedBy = "brand",cascade=CascadeType.ALL)
    List<VehicleEntity> vehicles;

    @JsonBackReference
    @OneToMany(mappedBy = "brand", cascade = CascadeType.ALL)
    List<ModelEntity> models;

}
