package az.ingress.turboaz.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@Entity
@Builder
@Table(name="models")
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ModelEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;

    @JsonBackReference
    @OneToMany(mappedBy = "model",cascade = CascadeType.ALL)
    List<VehicleEntity> vehicles;

    @ManyToOne
    @JoinColumn(name="brand_id")
    BrandEntity brand;
}
