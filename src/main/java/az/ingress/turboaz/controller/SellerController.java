package az.ingress.turboaz.controller;

import az.ingress.turboaz.dto.request.SellerRequestDto;
import az.ingress.turboaz.dto.response.SellerResponseDto;
import az.ingress.turboaz.service.SellerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Tag(name = "SellerController")
@RequestMapping("/seller")
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class SellerController {

    SellerService sellerService;

    @GetMapping
    @Operation(summary = "retrieve all Sellers")
    public List<SellerResponseDto> getAllSellers() {
        return sellerService.getAllSellers();
    }

    @GetMapping("/{id}")
    @Operation(summary = "get Seller by id")
    public SellerResponseDto getSellerById(@PathVariable Long id) {
        return sellerService.getSellerById(id);
    }

    @PostMapping
    @Operation(summary = "create Seller")
    public SellerResponseDto createSeller(@RequestBody SellerRequestDto sellerRequestDto) {
        return sellerService.createSeller(sellerRequestDto);
    }

    @PutMapping("/{id}")
    @Operation(summary = "update Seller")
    public SellerResponseDto updateSeller(@PathVariable Long id, @RequestBody SellerRequestDto sellerRequestDto) {
        return sellerService.updateSeller(id, sellerRequestDto);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "delete Seller")
    public void deleteSellerById(@PathVariable Long id) {
        sellerService.deleteSellerById(id);
    }

}
