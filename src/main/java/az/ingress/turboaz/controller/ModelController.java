package az.ingress.turboaz.controller;

import az.ingress.turboaz.dto.request.ModelRequestDto;
import az.ingress.turboaz.dto.response.ModelResponseDto;
import az.ingress.turboaz.service.ModelService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Tag(name = "ModelController")
@RequestMapping("/models")
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ModelController {

    ModelService modelService;

    @GetMapping
    @Operation(summary = "retrieve all Models")
    public List<ModelResponseDto> getAllModels() {
        return modelService.getAllModels();
    }

    @GetMapping("/{id}")
    @Operation(summary = "get Model by id")
    public ModelResponseDto getModelById(@PathVariable Long id) {
        return modelService.getModelById(id);
    }

    @PostMapping
    @Operation(summary = "create Model")
    public ModelResponseDto createModel(@RequestBody ModelRequestDto modelRequestDto) {
        return modelService.createModel(modelRequestDto);
    }

    @PutMapping("/{id}")
    @Operation(summary = "update Model")
    public ModelResponseDto updateModel(@PathVariable Long id, @RequestBody ModelRequestDto modelRequestDto) {
        return modelService.updateModel(id, modelRequestDto);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "delete Model")
    public void deleteModelById(@PathVariable Long id) {
        modelService.deleteModelById(id);
    }

}
