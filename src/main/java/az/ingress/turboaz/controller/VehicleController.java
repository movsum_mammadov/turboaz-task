package az.ingress.turboaz.controller;

import az.ingress.turboaz.dto.request.VehicleRequestDto;
import az.ingress.turboaz.dto.request.VehicleSearchDto;
import az.ingress.turboaz.dto.response.VehicleResponseDto;
import az.ingress.turboaz.dto.response.VehicleShortResponseDto;
import az.ingress.turboaz.service.VehicleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@Tag(name = "VehicleController")
@RequestMapping("/vehicles")
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class VehicleController {

    VehicleService vehicleService;

    @GetMapping
    @Operation(summary = "retrieve all Vehicles")
    public Page<VehicleResponseDto> getAllVehicles(@RequestParam(defaultValue = "0") Integer pageNumber,
                                                   @RequestParam(defaultValue = "5") Integer size) {
        return vehicleService.getAllVehicles(PageRequest.of(pageNumber, size));
    }

    @GetMapping("/{id}")
    @Operation(summary = "get Vehicle by id")
    public VehicleResponseDto getVehicleById(@PathVariable Long id) {
        return vehicleService.getVehicleById(id);
    }

    @PostMapping
    @Operation(summary = "create Vehicle")
    public VehicleResponseDto createVehicle(@RequestBody VehicleRequestDto vehicleRequestDto) {
        return vehicleService.createVehicle(vehicleRequestDto);
    }

    @PutMapping("/{id}")
    @Operation(summary = "update Vehicle")
    public VehicleResponseDto updateVehicle(@PathVariable Long id, @RequestBody VehicleRequestDto vehicleRequestDto) {
        return vehicleService.updateVehicle(id, vehicleRequestDto);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "delete Vehicle")
    public void deleteVehicleById(@PathVariable Long id) {
        vehicleService.deleteVehicleById(id);
    }

    @PostMapping("/search")
    @Operation(summary = "search Vehicle")
    public Page<VehicleShortResponseDto> search(@RequestBody VehicleSearchDto vehicleSearchDto,
                                                @RequestParam(defaultValue = "0") int pageNumber,
                                                @RequestParam(defaultValue = "5") int size) {
        return vehicleService.search(PageRequest.of(pageNumber, size), vehicleSearchDto);
    }

    @GetMapping("/date")
    @Operation(summary = "Vehicle is descending create date")
    public Page<VehicleShortResponseDto> getVehicleSortedByCreateDateDesc(@RequestParam(defaultValue = "0") int pageNumber,
                                                                          @RequestParam(defaultValue = "5") int size) {
        return vehicleService.getVehicleSortedByCreatedDateDesc(PageRequest.of(pageNumber, size));
    }

    @GetMapping("/highPrice")
    @Operation(summary = "Vehicle is ascending price")
    public Page<VehicleShortResponseDto> getVehicleSortedByPriceAsc(@RequestParam(defaultValue = "0") int pageNumber,
                                                                     @RequestParam(defaultValue = "5") int size) {
        return vehicleService.getVehicleSortedByPriceAsc(PageRequest.of(pageNumber, size));
    }

    @GetMapping("/lowPrice")
    @Operation(summary = "Vehicle is descending price")
    public Page<VehicleShortResponseDto> getVehicleSortedByPriceDesc(@RequestParam(defaultValue = "0") int pageNumber,
                                                                    @RequestParam(defaultValue = "5") int size) {
        return vehicleService.getVehicleSortedByPriceDesc(PageRequest.of(pageNumber, size));
    }

    @GetMapping("/mileage")
    @Operation(summary = "Vehicle is ascending mileage")
    public Page<VehicleShortResponseDto> getVehicleSortedByMileageAsc(@RequestParam(defaultValue = "0") int pageNumber,
                                                                      @RequestParam(defaultValue = "5") int size) {
        return vehicleService.getVehicleSortedByMileageAsc(PageRequest.of(pageNumber, size));
    }

    @GetMapping("/year")
    @Operation(summary = "Vehicle is descending year")
    public Page<VehicleShortResponseDto> getVehicleSortedByYearDesc(@RequestParam(defaultValue = "0") int pageNumber,
                                                                    @RequestParam(defaultValue = "5") int size) {
        return vehicleService.getVehicleSortedByYearDesc(PageRequest.of(pageNumber, size));
    }

}
