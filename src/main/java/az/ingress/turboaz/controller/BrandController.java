package az.ingress.turboaz.controller;

import az.ingress.turboaz.dto.request.BrandRequestDto;
import az.ingress.turboaz.dto.response.BrandResponseDto;
import az.ingress.turboaz.service.BrandService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Tag(name = "BrandController")
@RequestMapping("/brands")
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class BrandController {

    BrandService brandService;

    @GetMapping
    @Operation(summary = "retrieve all Brands")
    public List<BrandResponseDto> getAllBrands() {
        return brandService.getAllBrands();
    }

    @GetMapping("/{id}")
    @Operation(summary = "get Brand by id")
    public BrandResponseDto getBrandById(@PathVariable Long id) {
        return brandService.getBrandById(id);
    }

    @PostMapping
    @Operation(summary = "create Brand")
    public BrandResponseDto createBrand(@RequestBody BrandRequestDto brandRequestDto) {
        return brandService.createBrand(brandRequestDto);
    }

    @PutMapping("/{id}")
    @Operation(summary = "update Brand")
    public BrandResponseDto updateBrand(@PathVariable Long id, @RequestBody BrandRequestDto brandRequestDto) {
        return brandService.updateBrand(id, brandRequestDto);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "delete Brands")
    public void deleteBrandById(@PathVariable Long id) {
        brandService.deleteBrandById(id);
    }

}
