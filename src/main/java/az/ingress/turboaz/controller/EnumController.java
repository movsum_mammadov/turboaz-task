package az.ingress.turboaz.controller;

import az.ingress.turboaz.enums.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@Tag(name = "EnumController")
@RequestMapping("/enums")
public class EnumController {

    @GetMapping("/banTypes")
    public List<String> getBanTypes() {
        return Arrays.stream(BanTypeEnum.values()).map(BanTypeEnum::getValue).toList();
    }

    @GetMapping("/cities")
    public List<String> getCities() {
        return Arrays.stream(CityEnum.values()).map(CityEnum::getValue).toList();
    }

    @GetMapping("/colors")
    public List<String> getColors() {
        return Arrays.stream(ColorEnum.values()).map(ColorEnum::getValue).toList();
    }

    @GetMapping("/currencies")
    public List<String> getCurrencies() {
        return Arrays.stream(CurrencyEnum.values()).map(CurrencyEnum::getValue).toList();
    }

    @GetMapping("/enginesVolume")
    public List<Integer> getEngineVolumes() {
        return Arrays.stream(EngineVolumeEnum.values()).map(EngineVolumeEnum::getVolume).toList();
    }

    @GetMapping("/fuelTypes")
    public List<String> getFuelTypes() {
        return Arrays.stream(FuelTypeEnum.values()).map(FuelTypeEnum::getValue).toList();
    }

    @GetMapping("/gears")
    public List<String> getGears() {
        return Arrays.stream(GearEnum.values()).map(GearEnum::getValue).toList();
    }

    @GetMapping("/marketPlace")
    public List<String> getMarketPlace() {
        return Arrays.stream(MarketPlaceEnum.values()).map(MarketPlaceEnum::getValue).toList();
    }

    @GetMapping("/ownerCount")
    public List<String> getOwnerCount() {
        return Arrays.stream(OwnerCountEnum.values()).map(OwnerCountEnum::getValue).toList();
    }

    @GetMapping("/numberOfSeats")
    public List<String> getNumOfSeats() {
        return Arrays.stream(NumOfSeatsEnum.values()).map(NumOfSeatsEnum::getValue).toList();
    }

    @GetMapping("/sellerTypes")
    public List<String> getSellerTypes() {
        return Arrays.stream(SellerTypeEnum.values()).map(SellerTypeEnum::getValue).toList();
    }

    @GetMapping("/statuses")
    public List<String> getStatuses() {
        return Arrays.stream(StatusEnum.values()).map(StatusEnum::getValue).toList();
    }

    @GetMapping("/transmissions")
    public List<String> getTransmissions() {
        return Arrays.stream(TransmissionEnum.values()).map(TransmissionEnum::getValue).toList();
    }

}
